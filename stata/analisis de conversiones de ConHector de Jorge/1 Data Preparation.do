/*------------------------------------------------------------------------------*
| Title: 			Data preparation											|
| Project: 			ConHector									   				|
| Authors:			Jorge Zavala							                    |
| 					  									                        |
|																				|
| Description:		This .do imports, cleans and prepares data for analysis 	|
|                                                                               |
| Date Created: 06/04/2020			 					                        |										          
|																			    |
| Version: Stata 13/15 	                    							 	    |
*-------------------------------------------------------------------------------*/

/*--------------------------*
*           INDEX           *
*---------------------------*

	I. Survey data
	II. Sales Conversion data

*-------------------------------------------------------------------------------*/

*-----------------------*
*       DIRECTORY       *
*-----------------------*
	clear all
	set more off
	version 13
	
*global a "C:\Users\j.zavaladelgado\Dropbox (Personal)\Research & Consulting\2 Consulting\FundaK\12 ConHector"
*global a "C:\Users\Jorge Zavala\Dropbox (Personal)\Research & Consulting\2 Consulting\FundaK\12 ConHector"
global a "C:\Users\jzava\Dropbox (Personal)\Research & Consulting\2 Consulting\FundaK\12 ConHector"
*global a "C:\Users\jzavala\Dropbox\Research & Consulting\2 Consulting\FundaK\12 ConHector"


*---------------------------*
*		I. Survey data		*
*---------------------------*

*-----------------------*
*		Trujillo		*
*-----------------------*
clear all
import excel "$a\1 Data\1 Raw\ConHector - Maqueta Base de Datos.xlsx", sheet("RAW (Trujillo)") cellrange(A1:IM260) firstrow

	keep id_ind	cedula id_hh muni area barrio phone phone2 address 		///
			dw_ownership estrato proyect proyect_coded proyect_stage latitude 	///
			longitude altitude cedula_type civil_status hh_head_yn surveyed_yn 	///
			name1 name2 lastname1 lastname2 birth_date dpto_birth muni_birth 	///
			sex indigenous_yn relationship educ_level educ_grade literacy 		///
			numeracy study_yn study_no_rzn food_security_yn health_security_yn 	///
			id_ind2 wall floor rooms bedrooms_num kitchen drainage elect_yn 	///
			fuel risk assets bedrooms_equip kitchen_equip dw_ownership2 		///
			dw_price ocupation_prim ocupation_sec ocupation_hold 				///
			ocupation_norem ocupation_seek employment salary

	sort id_ind id_hh surveyed_yn
	encode id_hh, gen(id_hh_code)
	order id_hh_code, after(id_hh)
	
	replace area="1" if area=="Centro Poblado"
	replace area="0" if area=="Rural disperso"
	destring area, replace	
	label def area_lb 1 "Centro poblado" 0 "Rural disperso", replace
	label val area area_lb
	
	replace dw_ownership = "1" if dw_ownership=="Colectiva" | dw_ownership=="De un amigo" | dw_ownership=="De un familiar" | dw_ownership=="Ocupada de hecho" | dw_ownership=="Posesion sin titulo"
	replace dw_ownership = "2" if dw_ownership=="En arriendo o subarriendo" | dw_ownership=="En usufructo"
	replace dw_ownership = "3" if dw_ownership=="Propia, totalmente pagada"
	destring dw_ownership, replace
	label def dw_ownership_lb 1 "Ocupada" 2 "Alquilada" 3 "Propia", replace
	label val dw_ownership dw_ownership_lb
	
	replace estrato = 1 if estrato==0
	replace estrato = 2 if  estrato==3
	recode estrato (1=2) (2=1)
	label def estrato_lb 1 "NSE medio" 2 "NSE bajo", replace
	label val estrato estrato_lb
	
	encode proyect_coded, gen(proyect_coded2)
	order  proyect_coded2, after(proyect_coded)
	drop proyect_coded
	rename proyect_coded2 proyect_coded
	
	replace proyect_stage = "1" if proyect_stage=="EMPRENDIMIENTO"
	replace proyect_stage = "2" if proyect_stage=="FORTALECIMIENTO"
	replace proyect_stage = "" if proyect_stage=="#N/D"
	destring proyect_stage, replace
	label def proyect_stage_lb 1 "Emprendimiento" 2 "Fortalecimiento", replace
	label val proyect_stage proyect_stage_lb
	
	replace civil_status = "" if civil_status=="No reporta"
	encode civil_status, gen(civil_status_coded)
	order civil_status_coded, after(civil_status)
	
	replace hh_head_yn = "0" if hh_head_yn=="NO"
	replace hh_head_yn = "1" if hh_head_yn=="SI"
	destring hh_head_yn, replace
	
	replace surveyed_yn = "0" if surveyed_yn=="NO"
	replace surveyed_yn = "1" if surveyed_yn=="SI"
	destring surveyed_yn, replace
	
	gen birth_year=substr(birth_date,7,4), after(birth_date)
	destring birth_year, replace
	gen age=2020-birth_year, after(birth_year)
	
	replace sex = "0" if sex=="Hombre"
	replace sex = "1" if sex=="Mujer"
	destring sex, replace
	label def sex_lb 0 Men 1 Women, replace
	label val sex sex_lb
	
	replace indigenous_yn = "0" if indigenous_yn==""
	replace indigenous_yn = "1" if indigenous_yn=="Buena Vista"
	destring indigenous_yn, replace
	
	replace educ_level = "1" if educ_level=="Ninguno" | educ_level=="Preescolar" | educ_level=="Curso corto sena horas"
	replace educ_level = "1" if educ_level=="Basica Primaria"
	replace educ_level = "2" if educ_level=="Basica Secundaria"
	replace educ_level = "3" if educ_level=="Media"
	replace educ_level = "4" if educ_level=="Profesional" | educ_level=="Tecnica Profesional" | educ_level=="Tecnologico"
	destring educ_level, replace
	label def educ_lb 0 "Sin educación" 1 "Educación primaria" 2 "Educación secundaria" 3 "Educación media" 4 "Educación profesional", replace
	label val educ_level educ_lb
	
	replace literacy = "0" if literacy=="NO"
	replace literacy = "1" if literacy=="SI"
	destring literacy, replace
	
	replace numeracy = "0" if numeracy=="NO"
	replace numeracy = "1" if numeracy=="SI"
	destring numeracy, replace
	
	replace study_yn="0" if study_yn=="NO"
	replace study_yn="1" if study_yn=="SI"
	destring study_yn, replace
	
	replace food_security_yn = "1" if food_security_yn=="SI"
	replace food_security_yn = "0" if food_security_yn!="1"
	destring food_security_yn, replace
	
	gen 	wall_coded = 1 if wall=="Bahareque revocado" | wall=="Bahareque sin revocar" | wall=="Guadua, caña, esterilla, otro vegetal", after(wall)
	replace wall_coded = 2 if wall=="Madera burda, tabla, tablon" | wall=="Madera pulida" | wall=="Material prefabricado"
	replace wall_coded = 3 if wall=="Bloque, ladrillo, piedra" | wall=="Concreto Vaciado"
	label def wall_lb 1 "Bahareque, vegetal" 2 "Madera o prefabricado" 3 "Ladrillo, piedra o concreto", replace
	label val wall_coded wall_lb
	
	gen		floor_coded = 1 if floor=="Madera burda, tabla, tablon, otro vegetal", after(floor)
	replace floor_coded = 2 if floor=="Cemento, gravilla"
	replace floor_coded = 3 if floor=="Madera pulida y lacada, parque" | floor=="Baldosa, vinilo, tableta, ladrillo, laminado"
	label def floor_lb 1 "Tabla, vegetal" 2 "Cemento" 3 "Madera, baldosa", replace
	label val floor_coded floor_lb
	
	gen		drainage_coded = 1 if drainage=="Bajamar" | drainage=="Letrina" | drainage=="Inodoro sin conexion", after(drainage)
	replace drainage_coded = 2 if drainage=="Inodoro conectado a pozo septico"
	replace drainage_coded = 3 if drainage=="Inodoro conectado a alcantarillado"
	label def drainage_lb 1 "Sin conexión" 2 "Conexión a pozo" 3 "Alcantarillado", replace
	label val drainage_coded drainage_lb
			
	gen 	fuel_coded = 1 if fuel=="Leña, madera o carbon de leña", after(fuel)
	replace fuel_coded = 2 if fuel_coded!=1 & fuel!="Sin seleccionar"
	label def fuel_lb 1 "Leña, carbón" 2 "Gas", replace
	label val fuel_coded fuel_lb
	
	gen		work_yn = 1 if ocupation_prim=="Trabajando - Si marca la opcion (a) - Pase a la pregunta 7", before(ocupation_prim)
	replace work_yn = 0 if work_yn!=1
	label def yn 1 Sí 0 No, replace
	label val work_yn yn
	
	
	gen age_dumout = age
	replace age_dumout = 0 if age==.
	gen age_dumout_dum = 0
	replace age_dumout_dum = 1 if age==.
	
	preserve
	keep if surveyed_yn==1
save "$a\1 Data\2 Working\ConHector_SurveyTrujillo_aux1.dta", replace
	restore	
	gen n=1
	collapse (sum) hh_members=n (mean) hh_age=age hh_sex=sex 					///
			hh_educ_level=educ_level hh_literacy=literacy hh_numeracy=numeracy 	///
			hh_study_yn=study_yn hh_work_yn=work_yn, by(id_hh_code)
save "$a\1 Data\2 Working\ConHector_SurveyTrujillo_aux2.dta", replace

use "$a\1 Data\2 Working\ConHector_SurveyTrujillo_aux1.dta", clear
	merge 1:1 id_hh_code using "$a\1 Data\2 Working\ConHector_SurveyTrujillo_aux2.dta"
	drop _merge
	
	egen hh_wealth_index = rowmean(wall_coded floor_coded drainage_coded fuel_coded)
	gen dependency = hh_work_yn/hh_members
	xtile dependency_q3 = dependency, n(3)
	gen	overcrowd = hh_members/bedrooms_num
	
	gen dpto = 1
	label def dpto_lb 1 Trujillo 2 Tulua
	label val dpto dpto_lb
	
save "$a\1 Data\2 Working\ConHector_SurveyTrujillo.dta", replace
	
	
	
*-------------------*
*		Tulua		*
*-------------------*
clear all
import excel "$a\1 Data\1 Raw\ConHector - Maqueta Base de Datos.xlsx", sheet("RAW (Tuluá 2)") cellrange(A1:EQ51) firstrow

	keep id_ind id_hh cedula_type cedula sex birth_date educ_level educ_grade 	///
			study_yn study_no_rzn proyect_yn proyect_type proyect_stage 		///
			ocupation health dw_type dw_ownership wall floor roof kitchen 		///
			drainage elect_yn fuel hh_members hh_members_u5 hh_members_618 		///
			hh_members_618_school_yn hh_members_60 sex_member1 					///
			educ_level_member1 birth_year_member1 sex_member2 					///
			educ_level_member2 birth_year_member2 sex_member3 					///
			educ_level_member3 birth_year_member3 birth_year_member4 			///
			sex_member4 educ_level_member4 birth_year_member5 sex_member5 		///
			educ_level_member5 birth_year_member6 sex_member6 					///
			educ_level_member6 birth_year_member7 sex_member7 					///
			educ_level_member7 birth_year_member8 sex_member8 educ_level_member8 

	sort id_ind id_hh
	encode id_hh, gen(id_hh_code)
	order id_hh_code, after(id_hh)
	
	gen area=1
	label def area_lb 1 "Centro poblado" 0 "Rural disperso", replace
	label val area area_lb
	
	replace dw_ownership = "1" if dw_ownership=="f) De un familiar" | dw_ownership=="NULL"
	replace dw_ownership = "2" if dw_ownership=="c) En arriendo o subarriendo"
	replace dw_ownership = "3" if dw_ownership=="b) Propia, la están pagando" | dw_ownership=="a) Propia, totalmente pagada"
	destring dw_ownership, replace
	label def dw_ownership_lb 1 "Ocupada" 2 "Alquilada" 3 "Propia", replace
	label val dw_ownership dw_ownership_lb
	
	gen estrato = 1
	
	gen proyect_coded = 1 if proyect_type=="AGROPECUARIO"
	replace proyect_coded = 2 if proyect_type=="ALIMENTOS"
	replace proyect_coded = 5 if proyect_type=="COMERCIALIZACION"
	*encode proyect_coded, gen(proyect_coded2)
	*order  proyect_coded2, after(proyect_coded)
	*drop proyect_coded
	*rename proyect_coded2 proyect_coded
	
	replace proyect_yn ="0" if proyect_yn=="No"
	replace proyect_yn ="1" if proyect_yn=="Sí"
	destring proyect_yn, replace
	
	
	replace proyect_stage = "1" if proyect_stage=="EMPRENDIMIENTO"
	replace proyect_stage = "2" if proyect_stage=="FORTALECIMIENTO"
	replace proyect_stage = "" if proyect_stage=="NULL"
	destring proyect_stage, replace
	
	*replace civil_status = "" if civil_status=="No reporta"
	*encode civil_status, gen(civil_status_coded)
	*order civil_status_coded, after(civil_status)
	
	*replace hh_head_yn = "0" if hh_head_yn=="NO"
	*replace hh_head_yn = "1" if hh_head_yn=="SI"
	*destring hh_head_yn, replace
	
	*replace surveyed_yn = "0" if surveyed_yn=="NO"
	*replace surveyed_yn = "1" if surveyed_yn=="SI"
	*destring surveyed_yn, replace
	
	gen birth_year=substr(birth_date,7,4), after(birth_date)
	destring birth_year, replace
	gen age=2020-birth_year, after(birth_year)
	replace age=29 if age==1
	
	replace sex = "0" if sex=="Hombre"
	replace sex = "1" if sex=="Mujer"
	destring sex, replace
	label def sex_lb 0 Men 1 Women, replace
	label val sex sex_lb
	
	*replace indigenous_yn = "0" if indigenous_yn==""
	*replace indigenous_yn = "1" if indigenous_yn=="Buena Vista"
	*destring indigenous_yn, replace
	
	replace educ_level = "1" if educ_level=="Ninguno" | educ_level=="Preescolar" | educ_level=="Curso corto sena horas"
	replace educ_level = "1" if educ_level=="Basica Primaria (1o. a 5to.)"
	replace educ_level = "2" if educ_level=="Basica Secundaria (6to. a 9no.)"
	replace educ_level = "3" if educ_level=="Educacion Media (1 y 11)"
	replace educ_level = "4" if educ_level=="Educacion Tecnica" | educ_level=="Educacion Tecnologica" | educ_level=="Educacion Universitaria" | educ_level=="Posgrado"
	destring educ_level, replace
	
	*replace literacy = "0" if literacy=="NO"
	*replace literacy = "1" if literacy=="SI"
	*destring literacy, replace
	
	*replace numeracy = "0" if numeracy=="NO"
	*replace numeracy = "1" if numeracy=="SI"
	*destring numeracy, replace
	
	replace study_yn="0" if study_yn=="No"
	replace study_yn="1" if study_yn=="Si"
	destring study_yn, replace
	
	*replace food_security_yn = "1" if food_security_yn=="SI"
	*replace food_security_yn = "0" if food_security_yn!="1"
	*destring food_security_yn, replace
	
	gen 	wall_coded = 1 if wall=="" | wall=="Bahareque" | wall=="Zinc, tela, lona, cartón, latas, plásticos" | wall=="Guadua, caña, esterilla, otro vegetal", after(wall)
	replace wall_coded = 2 if wall=="Madera burda, tabla, tablon" | wall=="Tapia pisada, adobe" | wall=="Material prefabricado"
	replace wall_coded = 3 if wall=="Bloque, ladrillo, piedra, madera pulida" | wall=="Concreto Vaciado"
	
	gen		floor_coded = 1 if floor=="Otro" | floor=="", after(floor)
	replace floor_coded = 2 if floor=="Cemento, gravilla"
	replace floor_coded = 3 if floor=="Madera pulida y lacada, parque" | floor=="Baldosa, vinilo, tableta, ladrillo"
	
	gen		drainage_coded = 1 if drainage=="" | drainage=="Letrina" | drainage=="Inodoro sin conexion", after(drainage)
	replace drainage_coded = 2 if drainage=="Con conexión pozo séptico"
	replace drainage_coded = 3 if drainage=="Con conexión a alcantarillado"
			
	gen 	fuel_coded = 1 if fuel=="Material de desecho, leña, carbón de leña", after(fuel)
	replace fuel_coded = 2 if fuel_coded!=1 & fuel!="Sin seleccionar"
	
	gen		work_yn = 0 if ocupation=="No tiene ingresos"
	replace work_yn = 1 if work_yn!=0
	
	
	gen age_dumout = age
	replace age_dumout = 0 if age==.
	gen age_dumout_dum = 0
	replace age_dumout_dum = 1 if age==.
	
	replace birth_year_member1="" if birth_year_member1=="Ingridtatiana"
	destring birth_year_member1, replace
	gen age_member1=2020-birth_year_member1
	
	replace birth_year_member2=1999 if birth_year_member2==19999
	replace birth_year_member2=1986 if birth_year_member2==86
	replace birth_year_member2=2009 if birth_year_member2==9
	gen age_member2=2020-birth_year_member2
	
	gen age_member3=2020-birth_year_member3
	gen age_member4=2020-birth_year_member4
	gen age_member5=2020-birth_year_member5
	gen age_member6=2020-birth_year_member6
	gen age_member7=2020-birth_year_member7
	gen age_member8=2020-birth_year_member8
	
	egen hh_age=rowmean(age age_member1 age_member2 age_member3 age_member4 age_member5 age_member6 age_member7 age_member8)
	egen hh_sex=rowmean(sex sex_member1 sex_member2 sex_member3 sex_member4 sex_member5 sex_member6 sex_member7 sex_member8)
	egen hh_educ_level=rowmean(educ_level educ_level_member1 educ_level_member2 educ_level_member3 educ_level_member4 educ_level_member5 educ_level_member6 educ_level_member7 educ_level_member8)

	egen hh_wealth_index = rowmean(wall_coded floor_coded drainage_coded fuel_coded)
	*gen dependency = hh_work_yn/hh_members
	*xtile dependency_q3 = dependency, n(3)
	*gen	overcrowd = hh_members/bedrooms_num
	
	gen dpto=2

save "$a\1 Data\2 Working\ConHector_SurveyTulua.dta", replace


*-----------------------------------*
*		Append Survey datasets		*
*-----------------------------------*

use "$a\1 Data\2 Working\ConHector_SurveyTrujillo.dta", clear
	append using "$a\1 Data\2 Working\ConHector_SurveyTulua.dta", force
save "$a\1 Data\2 Working\ConHector_Survey.dta", replace



*---------------------------------------*
*		II. Sales convertion data		*
*---------------------------------------*
clear all
import delimited "$a\1 Data\1 Raw\ConHector - Reportes_Conversión de Ofertas_Table", varnames(1) case(preserve)
	
	rename ïid_ind id_ind
	drop if id_ind=="null" | id_ind=="1"
	destring id_ind, replace
	encode sale_type, gen(sale_type_coded)
	drop outcome user phone sale_name sale_type date
	rename followup conversion
	replace conversion = "1" if conversion=="CONCRETADO" | conversion=="ACEPTADO"
	replace conversion = "0" if conversion!="1"
	destring conversion, replace
	sort id_ind id_sale conversion	
	bys id_ind id_sale: gen rep=_n
	bys id_ind id_sale: gen Rep=_N
	drop if Rep==2 & rep==1
	drop Rep rep
	
	gen sale_type_comercio = 0
	replace sale_type_comercio = 1 if sale_type_coded==1
	gen sale_type_financiero = 0
	replace sale_type_financiero = 1 if sale_type_coded==2
	gen sale_type_hogar = 0
	replace sale_type_hogar = 1 if sale_type_coded==3
	gen sale_type_laboral = 0
	replace sale_type_laboral = 1 if sale_type_coded==4
	gen sale_type_legal = 0
	replace sale_type_legal = 1 if sale_type_coded==5	
	gen sale_type_salud = 0
	replace sale_type_salud = 1 if sale_type_coded==6
	drop sale_type_coded
	
	
	*	reshape wide conversion, i(id_ind) j(id_sale)

	
	foreach x in sale_type_comercio sale_type_financiero sale_type_hogar sale_type_laboral sale_type_legal sale_type_salud {
		gen	`x'_conv = `x'*conversion
	}
	
	
	gen sales=1
	collapse (sum) sales_total=sales conversion_total=conversion (mean) conversion_mean=conversion (sum) sale_type*, by(id_ind)
	
	foreach x in sale_type_comercio sale_type_financiero sale_type_hogar sale_type_laboral sale_type_legal sale_type_salud {
		gen `x'_conv_rate = `x'_conv/`x'
	}
	
	
save "$a\1 Data\2 Working\ConHector_SalesConversion.dta", replace
	
*---------------------------------------*
*		III. Bot interaction data		*
*---------------------------------------*
import delimited "$a\1 Data\1 Raw\ConHector - Reportes_Bot_Table.csv", varnames(1) clear 
rename ïid_ind id_ind
drop if id_ind=="null"
destring id_ind, replace
gen bot = 1
collapse (sum) bot, by(id_ind)

save "$a\1 Data\2 Working\ConHector_BotInteraction.dta", replace

*-----------------------------------------------*
*		IV. Call Center interaction data		*
*-----------------------------------------------*
import delimited "$a\1 Data\1 Raw\ConHector - Reportes_Operación del Call Center_Table.csv", varnames(1) clear 
rename ïid_ind id_ind
drop if id_ind=="null"
destring id_ind, replace
gen callcenter = 1
collapse (sum) callcenter, by(id_ind)


save "$a\1 Data\2 Working\ConHector_CallCenterInteraction.dta", replace

*-------------------------------*
*		V. Merge dataset		*
*-------------------------------*
use "$a\1 Data\2 Working\ConHector_Survey.dta", clear

	merge 1:1 id_ind using "$a\1 Data\2 Working\ConHector_SalesConversion.dta"
	drop if _merge==2
	rename _merge benef
	replace benef = 0 if benef==1
	replace benef = 1 if benef==3
	label def benef_lb 0 "No sales offered" 1 "Offered sales", replace
	label val benef benef_lb

	xtile conversion_mean_q2=conversion_mean, n(2)
	recode conversion_mean_q2 (1=0) (2=1)
	
	merge 1:1 id_ind using "$a\1 Data\2 Working\ConHector_BotInteraction.dta"
	drop if _merge==2
	rename _merge basebot
	replace basebot = 0 if basebot==1
	replace basebot = 1 if basebot==3
	label def basebot_lb 0 "No bot interaction" 1 "Bot interaction", replace
	label val basebot basebot_lb
	
	merge 1:1 id_ind using "$a\1 Data\2 Working\ConHector_CallCenterInteraction.dta"
	drop if _merge==2
	rename _merge basecall
	replace basecall = 0 if basecall==1
	replace basecall = 1 if basecall==3
	label def basecall_lb 0 "No call center interaction" 1 "Call center interaction", replace
	label val basecall basecall_lb
	
save "$a\1 Data\3 Coded\ConHector_SurveyConversion.dta", replace
	



	
	
	reg conversion_mean i.estrato hh_educ_level hh_sex   			///
			i.dpto i.dw_ownership i.proyect_stage hh_wealth_index age_dumout age_dumout_dum 	///
			work_yn educ_level sex study_yn, vce(robust)

	
	
	reg conversion_mean i.estrato hh_educ_level hh_sex  			///
			i.dependency_q3 i.dw_ownership hh_wealth_index age_dumout age_dumout_dum 	///
			i.civil_status_coded literacy work_yn educ_level hh_head_yn 		///
			i.proyect_stage , vce(robust)


global controls area dw_ownership estrato proyect_stage hh_head_yn age sex 		///
		indigenous_yn educ_level literacy numeracy study_yn wall_coded 			///
		floor_coded drainage_coded fuel_coded work_yn hh_members 		///
		hh_age hh_sex hh_educ_level hh_literacy hh_numeracy hh_study_yn 		///
		hh_work_yn hh_wealth_index dependency overcrowd

global conversion sales_total conversion_total conversion_mean conversion_mean_q3 ///
		sale_type_comercio sale_type_financiero sale_type_hogar sale_type_laboral 		///
		sale_type_legal sale_type_salud sale_type_comercio_conv 						///
		sale_type_financiero_conv sale_type_hogar_conv sale_type_laboral_conv 			///
		sale_type_legal_conv sale_type_salud_conv sale_type_comercio_conv_rate 			///
		sale_type_financiero_conv_rate sale_type_hogar_conv_rate 						///
		sale_type_laboral_conv_rate sale_type_legal_conv_rate sale_type_salud_conv_rate

matpwcorr $controls $conversion $controls, gen

	drop if var1==""
	drop if corr==1

	gen variable1_control=0
	gen variable2_control=0
		foreach x of global controls { 
		replace variable1_control=1 if var1=="`x'"
		replace variable2_control=1 if var2=="`x'"
		}
		

	gen variable1_conversion=0
	gen variable2_conversion=0
		foreach x of global conversion { 
		replace variable1_conversion=1 if var1=="`x'"
		replace variable2_conversion=1 if var2=="`x'"
		}	
		
	keep var1 var2 corr pv variable1* variable2*
	keep if pv<0.1
	rename var1 Variable_1
	rename var2 Variable_2
	sort Variable_1 Variable_2
	rename corr Correlation
	rename pv P_value
	replace P_value=round(P_value,.0001)
	order Variable_1 Variable_2 Correlation P_value variable1* variable2*
	
export excel using "$a\3 Analysis\1 Tables\SumStats_ConHector.xlsx", sheet("Correlations") cell(B2) firstrow(var) sheetmodify
