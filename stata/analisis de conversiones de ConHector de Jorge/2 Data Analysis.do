/*------------------------------------------------------------------------------*
| Title: 			Data analysis												|
| Project: 			ConHector									   				|
| Authors:			Jorge Zavala							                    |
| 					  									                        |
|																				|
| Description:		This .do generate summary statistics and regresison analysis|
|                                                                               |
| Date Created: 13/04/2020			 					                        |										          
|																			    |
| Version: Stata 13/15 	                    							 	    |
*-------------------------------------------------------------------------------*/

/*--------------------------*
*           INDEX           *
*---------------------------*

*-------------------------------------------------------------------------------*/

*-----------------------*
*       DIRECTORY       *
*-----------------------*
	clear all
	set more off
	version 13
	
*global a "C:\Users\j.zavaladelgado\Dropbox (Personal)\Research & Consulting\2 Consulting\FundaK\12 ConHector"
*global a "C:\Users\Jorge Zavala\Dropbox (Personal)\Research & Consulting\2 Consulting\FundaK\12 ConHector"
global a "C:\Users\jzava\Dropbox (Personal)\Research & Consulting\2 Consulting\FundaK\12 ConHector"
*global a "C:\Users\jzavala\Dropbox\Research & Consulting\2 Consulting\FundaK\12 ConHector"

*-------------------------------*
*		Summary Statistics		*
*-------------------------------*

use "$a\1 Data\3 Coded\ConHector_SurveyConversion.dta", clear

	global sumstats area dw_ownership estrato proyect_stage age sex educ_level	///
					study_yn work_yn wall_coded floor_coded drainage_coded 		///
					fuel_coded hh_members hh_age hh_sex hh_educ_level 			///
					hh_wealth_index benef sales_total conversion_total 			///
					conversion_mean conversion_mean_q2 sale_type_comercio_conv 	///
					sale_type_financiero_conv sale_type_hogar_conv 				///
					sale_type_laboral_conv sale_type_legal_conv sale_type_salud_conv 					///
					sale_type_comercio_conv_rate sale_type_financiero_conv_rate ///
					sale_type_hogar_conv_rate sale_type_laboral_conv_rate 		///
					sale_type_legal_conv_rate sale_type_salud_conv_rate

 	putexcel set "$a\3 Analysis\1 Tables\Balance.xlsx", sheet("By Dpto") modify
	matrix define R = J(35, 5, .)
	
	local cont=1
	local cont2=4
	foreach x of global sumstats {
	
	putexcel B`cont2'=("`x'") 
	qui mean `x', over(dpto)
	matrix mean=e(b)
	local mean1=mean[1,1]
	local mean2=mean[1,2]
	qui reg `x' dpto, robust
		local obs=e(N)
		matrix b = e(b)
		matrix V = e(V)
		matrix df=e(df_r)
		local c_l = b[1,1]
		local c = string(`c_l', "%9.3f")
		local se_l = sqrt(V[1,1])
		local se = string(`se_l', "%9.3f")
		local d = df[1,1]
		local t = `c_l'/`se_l' 
		local pv = 2*ttail(`d',abs(`t'))
	
	matrix R[`cont', 1] = `mean1'
	matrix R[`cont', 2] = `mean2'
	matrix R[`cont', 3] = `mean1'-`mean2'
	matrix R[`cont', 4] = `pv'
	matrix R[`cont', 5] = `obs'
	
	local cont = `cont' + 1
	local cont2 = `cont2' + 1	
	}
	putexcel C4=matrix(R)

	global sumstats area dw_ownership estrato proyect_stage age sex educ_level	///
					study_yn work_yn wall_coded floor_coded drainage_coded 		///
					fuel_coded hh_members hh_age hh_sex hh_educ_level 			///
					hh_wealth_index sales_total conversion_total 			///
					conversion_mean conversion_mean_q2 sale_type_comercio_conv 	///
					sale_type_financiero_conv sale_type_hogar_conv 				///
					sale_type_laboral_conv sale_type_legal_conv sale_type_salud_conv 					///
					sale_type_comercio_conv_rate sale_type_financiero_conv_rate ///
					sale_type_hogar_conv_rate sale_type_laboral_conv_rate 		///
					sale_type_legal_conv_rate sale_type_salud_conv_rate

 	putexcel set "$a\3 Analysis\1 Tables\Balance.xlsx", sheet("By Beneficiary") modify
	matrix define R = J(34, 5, .)
	
	local cont=1
	local cont2=4
	foreach x of global sumstats {
	
	putexcel B`cont2'=("`x'") 
	qui mean `x', over(benef)
	matrix mean=e(b)
	local mean1=mean[1,1]
	local mean2=mean[1,2]
	qui reg `x' benef, robust
		local obs=e(N)
		matrix b = e(b)
		matrix V = e(V)
		matrix df=e(df_r)
		local c_l = b[1,1]
		local c = string(`c_l', "%9.3f")
		local se_l = sqrt(V[1,1])
		local se = string(`se_l', "%9.3f")
		local d = df[1,1]
		local t = `c_l'/`se_l' 
		local pv = 2*ttail(`d',abs(`t'))
	
	matrix R[`cont', 1] = `mean1'
	matrix R[`cont', 2] = `mean2'
	matrix R[`cont', 3] = `mean1'-`mean2'
	matrix R[`cont', 4] = `pv'
	matrix R[`cont', 5] = `obs'
	
	local cont = `cont' + 1
	local cont2 = `cont2' + 1	
	}
	putexcel C4=matrix(R)

	
*-------------------------------*
*		Descriptive graphs		*
*-------------------------------*
	xtile age_q4=age, n(4)
graph twoway lpolyci conversion_mean age if dpto==2
	
	

*-----------------------*
*		Regressions		*
*-----------------------*
use "$a\1 Data\3 Coded\ConHector_SurveyConversion.dta", clear

	pca floor_coded wall_coded fuel_coded drainage_coded, mine(1)
	predict hh_wealth_pca
	xtile hh_wealth_pca_q3=hh_wealth_pca, n(3)

	
	global controls_trujillo age_dumout age_dumout_dum sex i.educ_level work_yn indigenous_yn i.civil_status_coded i.proyect_stage i.dw_ownership ///
			hh_age hh_educ_level i.floor_coded i.wall_coded i.fuel_coded i.drainage_coded i.dependency_q3
	
	global controls_tulua age sex i.dw_ownership proyect_yn i.educ_level work_yn hh_age hh_sex hh_educ_level i.floor_coded i.wall_coded i.drainage_coded 
		
	global controls_pool age_dumout age_dumout_dum sex i.educ_level work_yn i.dw_ownership hh_age hh_sex hh_educ_level i.floor_coded i.wall_coded i.fuel_coded i.drainage_coded i.dpto

*Trujillo
	reg conversion_mean $controls_trujillo if dpto==1, r
	logit conversion_mean_q2 $controls_trujillo if dpto==1, r

	margins, at(work_yn=(0(1)1))
	marginsplot, plotopts(msize(small)) ciopts(msize(large) lstyle(p1mark)  			///
	lcolor(red)) graphregion(color(white)) 	///
	tit("Conversión de ofertas - Trujillo", span) xsize(6) ytit("Probabilidad de convertir 90%+ de ofertas" " ") xtit(" " "Tiene un trabajo remunerado") ylab(0(.1)1) 
	graph export "$a\3 Analysis\2 Graphs\marginsplot_PrConversionq2_Trujillo_work.png", replace
	
	margins, at(sex=(0(1)1))
	marginsplot, plotopts(msize(small)) ciopts(msize(large) lstyle(p1mark)  			///
	lcolor(red)) graphregion(color(white)) 	///
	tit("Conversión de ofertas - Trujillo", span) xsize(6) ytit("Probabilidad de convertir 90%+ de ofertas" " ") xtit(" " "Sexo") ylab(0(.1)1) 
	graph export "$a\3 Analysis\2 Graphs\marginsplot_PrConversionq2_Trujillo_sex.png", replace
	
	margins, at(hh_age=(10(10)80))
	marginsplot, plotopts(msize(small)) ciopts(msize(large) lstyle(p1mark)  			///
	lcolor(red)) graphregion(color(white)) 	///
	tit("Conversión de ofertas - Trujillo", span) xsize(6) ytit("Probabilidad de convertir 90%+ de ofertas" " ") xtit(" " "Edad promedio en el hogar") ylab(0(.1)1) 
	graph export "$a\3 Analysis\2 Graphs\marginsplot_PrConversionq2_Trujillo_hhage.png", replace
	
	
* Tulua	
	reg conversion_mean $controls_tulua if dpto==2, r
	logit conversion_mean_q2 $controls_tulua if dpto==2, r
	
	margins, at(age=(24(10)74))
	marginsplot, plotopts(msize(small)) ciopts(msize(large) lstyle(p1mark)  			///
	lcolor(red)) graphregion(color(white)) 	///
	tit("Conversión de ofertas - Tuluá", span) xsize(6) ytit("Probabilidad de convertir 90%+ de ofertas" " ") xtit(" " "Edad") ylab(0(.1)1) 
	graph export "$a\3 Analysis\2 Graphs\marginsplot_PrConversionq2_Tulua_age.png", replace
	
	margins, at(sex=(0(1)1))
	marginsplot, plotopts(msize(small)) ciopts(msize(large) lstyle(p1mark)  			///
	lcolor(red)) graphregion(color(white)) 	///
	tit("Conversión de ofertas - Tuluá", span) xsize(6) ytit("Probabilidad de convertir 90%+ de ofertas" " ") xtit(" " "Sexo") ylab(0(.1)1) 
	graph export "$a\3 Analysis\2 Graphs\marginsplot_PrConversionq2_Tulua_sex.png", replace
	
	margins, at(dw_ownership=(1(1)3))
	marginsplot, plotopts(msize(small)) ciopts(msize(large) lstyle(p1mark)  			///
	lcolor(red)) graphregion(color(white)) 	///
	tit("Conversión de ofertas - Tuluá", span) xsize(6) ytit("Probabilidad de convertir 90%+ de ofertas" " ") xtit(" " "Tipo de propiedad del hogar") ylab(0(.1)1) 
	graph export "$a\3 Analysis\2 Graphs\marginsplot_PrConversionq2_Tulua_dwownership.png", replace
	
	margins, at(educ_level=(1(1)4))
	marginsplot, plotopts(msize(small)) ciopts(msize(large) lstyle(p1mark)  			///
	lcolor(red)) graphregion(color(white)) 	///
	xlab(,labsize(small)) tit("Conversión de ofertas - Tuluá", span) xsize(6) ytit("Probabilidad de convertir 90%+ de ofertas" " ") xtit(" " "Nivel educativo") ylab(0(.1)1) 
	graph export "$a\3 Analysis\2 Graphs\marginsplot_PrConversionq2_Tulua_educlevel.png", replace

	
*Pool	
	reg conversion_mean $controls_pool,r
	logit conversion_mean_q2 $controls_pool, r
	
	margins, at(age_dumout=(23(10)73))
	marginsplot, plotopts(msize(small)) ciopts(msize(large) lstyle(p1mark)  			///
	lcolor(red)) graphregion(color(white)) 	///
	tit("Conversión de ofertas - General", span) xsize(6) ytit("Probabilidad de convertir 90%+ de ofertas" " ") xtit(" " "Edad") ylab(0(.1)1) 
	graph export "$a\3 Analysis\2 Graphs\marginsplot_PrConversionq2_Pool_age.png", replace

	margins, at(educ_level=(1(1)4))
	marginsplot, plotopts(msize(small)) ciopts(msize(large) lstyle(p1mark)  			///
	lcolor(red)) graphregion(color(white)) 	///
	tit("Conversión de ofertas - General", span) xsize(6) ytit("Probabilidad de convertir 90%+ de ofertas" " ") xtit(" " "Nivel educativo") ylab(0(.1)1) 
	graph export "$a\3 Analysis\2 Graphs\marginsplot_PrConversionq2_Pool_educlevel.png", replace

