

with age as (
SELECT TIMESTAMPDIFF(YEAR, birthdate, register_at) as age, birthdate, register_at, gender, country, Ahorro_Meta5, Ahorro_Valor6, Ahorro_Frecuencia6, Ahorro_Unidad6 FROM users  u
inner join historical_lessons_played hp  on u.id=hp.id
left join campaigns  c on u.campaign_id =c.id 
left join country_lu cl on c.country_id =cl.id 
where Ahorro_Meta5 is not null
)


select *,
  case
   when age between 0 and 19  then '0-19'
   when age between 20 and 29 then '20-29'
   when age between 30 and 39 then '30-39'
   when age between 40 and 49 then '40-49'
   when age between 50 and 64 then '50-64'
   when age between 60 and 110 then '65 +'
   
 END as age_range
 from age



