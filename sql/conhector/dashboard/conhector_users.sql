SELECT participants.id AS 'id', participants.participant AS 'idParticipante', participants.document AS 'Documento', 
CONCAT(participants.first_name, ' ', participants.last_name) AS 'NombreCompleto',
IF(participants.mobile_phone IS NULL, 'SIN TELEFONO', participants.mobile_phone) AS 'TelefonoMovil',
IF(participants.gender = "m", "Hombre", "Mujer") AS 'Genero',
TIMESTAMPDIFF(YEAR, participants.birthday, CURDATE()) AS 'Edad',
IF(TIMESTAMPDIFF(YEAR, participants.birthday, CURDATE()) BETWEEN 18 AND 29, 'Jóvenes (18-29)', IF(TIMESTAMPDIFF(YEAR, participants.birthday, CURDATE()) BETWEEN 30 AND 59, 'Adultos (30-59)', IF(TIMESTAMPDIFF(YEAR, participants.birthday, CURDATE()) >= 60, 'Adultos Mayores (60+)', 'Fuera de Rango'))) AS 'RangoEdad',
participants.family_id AS 'Familia',
IF(participants.is_contact = 1, 'Si', 'No') AS 'EsContacto',
participants.town AS 'Municipio', IF(participants.neighborhood IS NULL, 'SIN VEREDA', participants.neighborhood) AS 'Vereda',
DATE_FORMAT(CONVERT_TZ(surveys_imports.created_at, '+00:00', '-05:00'), '%Y%m%d') AS 'Registro',
surveys_imports.name_import AS 'Grupo'
FROM participants
    INNER JOIN participant_surveys ON participants.id = participant_surveys.participant_id
    INNER JOIN surveys_imports ON participant_surveys.survey_id = surveys_imports.id;