SELECT program_steps.program_id AS 'idOferta', programs.name AS 'NombreOferta',
COUNT(program_steps.id) AS 'TotalPasos', COUNT(program_steps.id) * 0.00076 AS 'CostoTotal',
programs.coverage AS 'Sprint',
DATE_FORMAT(programs.start_date, '%Y%m%d') AS 'FechaInicio', 
DATE_FORMAT(programs.end_date, '%Y%m%d') AS 'FechaFin'
FROM program_steps
    INNER JOIN programs ON program_steps.program_id = programs.id
GROUP BY program_steps.program_id;