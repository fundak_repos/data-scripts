SELECT 
    participants_programs_logs.participant_id AS 'idParticipante',
    CONCAT(participants.first_name,
            ' ',
            participants.last_name) AS 'NombreCompleto',
    participants.mobile_phone AS 'TelefonoMovil',
    IF(participants.gender = 'm',
        'Hombre',
        'Mujer') AS 'Genero',
    TIMESTAMPDIFF(YEAR,
        participants.birthday,
        CURDATE()) AS 'Edad',
    IF(TIMESTAMPDIFF(YEAR,
            participants.birthday,
            CURDATE()) BETWEEN 18 AND 29,
        'JÃ³venes (18-29)',
        IF(TIMESTAMPDIFF(YEAR,
                participants.birthday,
                CURDATE()) BETWEEN 30 AND 59,
            'Adultos (30-59)',
            IF(TIMESTAMPDIFF(YEAR,
                    participants.birthday,
                    CURDATE()) >= 60,
                'Adultos Mayores (60+)',
                'Fuera de Rango'))) AS 'RangoEdad',
    participants.town AS 'Municipio',
    IF(participants.neighborhood IS NULL,
        'SIN VEREDA',
        participants.neighborhood) AS 'Vereda',
    'digitall_per_sprint1' AS 'Intervencion',
    5 AS 'OpcionesDisponibles',
    3 AS 'Mediana',
    COUNT(participants_programs_logs.id) AS 'Interacciones',
    IF(COUNT(participants_programs_logs.id) < 3,
        'baja',
        IF(COUNT(participants_programs_logs.id) > 3,
            'alta',
            'media')) AS 'NivelDeInteraccion',
    surveys_imports.name_import AS 'Grupo'
FROM
    participants_programs_logs
        INNER JOIN
    participant_surveys ON participants_programs_logs.participant_id = participant_surveys.participant_id
        INNER JOIN
    participants ON participants_programs_logs.participant_id = participants.id
        INNER JOIN
    surveys_imports ON participant_surveys.survey_id = surveys_imports.id
WHERE
    participants_programs_logs.program_id BETWEEN 52 AND 56
        AND participants_programs_logs.log_state = 'program'
GROUP BY participants_programs_logs.participant_id 
UNION SELECT 
    participants_programs_logs.participant_id AS 'idParticipante',
    CONCAT(participants.first_name,
            ' ',
            participants.last_name) AS 'NombreCompleto',
    participants.mobile_phone AS 'TelefonoMovil',
    IF(participants.gender = 'm',
        'Hombre',
        'Mujer') AS 'Genero',
    TIMESTAMPDIFF(YEAR,
        participants.birthday,
        CURDATE()) AS 'Edad',
    IF(TIMESTAMPDIFF(YEAR,
            participants.birthday,
            CURDATE()) BETWEEN 18 AND 29,
        'JÃ³venes (18-29)',
        IF(TIMESTAMPDIFF(YEAR,
                participants.birthday,
                CURDATE()) BETWEEN 30 AND 59,
            'Adultos (30-59)',
            IF(TIMESTAMPDIFF(YEAR,
                    participants.birthday,
                    CURDATE()) >= 60,
                'Adultos Mayores (60+)',
                'Fuera de Rango'))) AS 'RangoEdad',
    participants.town AS 'Municipio',
    IF(participants.neighborhood IS NULL,
        'SIN VEREDA',
        participants.neighborhood) AS 'Vereda',
    'digitall_per_sprint2' AS 'Intervencion',
    7 AS 'OpcionesDisponibles',
    4 AS 'Mediana',
    COUNT(participants_programs_logs.id) AS 'Interacciones',
    IF(COUNT(participants_programs_logs.id) < 4,
        'baja',
        IF(COUNT(participants_programs_logs.id) > 4,
            'alta',
            'media')) AS 'NivelDeInteraccion',
    surveys_imports.name_import AS 'Grupo'
FROM
    participants_programs_logs
        INNER JOIN
    participant_surveys ON participants_programs_logs.participant_id = participant_surveys.participant_id
        INNER JOIN
    participants ON participants_programs_logs.participant_id = participants.id
        INNER JOIN
    surveys_imports ON participant_surveys.survey_id = surveys_imports.id
WHERE
    participants_programs_logs.program_id BETWEEN 101 AND 107
        AND participants_programs_logs.log_state = 'program'
GROUP BY participants_programs_logs.participant_id 
UNION SELECT 
    participants_programs_logs.participant_id AS 'idParticipante',
    CONCAT(participants.first_name,
            ' ',
            participants.last_name) AS 'NombreCompleto',
    participants.mobile_phone AS 'TelefonoMovil',
    IF(participants.gender = 'm',
        'Hombre',
        'Mujer') AS 'Genero',
    TIMESTAMPDIFF(YEAR,
        participants.birthday,
        CURDATE()) AS 'Edad',
    IF(TIMESTAMPDIFF(YEAR,
            participants.birthday,
            CURDATE()) BETWEEN 18 AND 29,
        'JÃ³venes (18-29)',
        IF(TIMESTAMPDIFF(YEAR,
                participants.birthday,
                CURDATE()) BETWEEN 30 AND 59,
            'Adultos (30-59)',
            IF(TIMESTAMPDIFF(YEAR,
                    participants.birthday,
                    CURDATE()) >= 60,
                'Adultos Mayores (60+)',
                'Fuera de Rango'))) AS 'RangoEdad',
    participants.town AS 'Municipio',
    IF(participants.neighborhood IS NULL,
        'SIN VEREDA',
        participants.neighborhood) AS 'Vereda',
    'digitall_mex_sprint1' AS 'Intervencion',
    6 AS 'OpcionesDisponibles',
    3 AS 'Mediana',
    COUNT(participants_programs_logs.id) AS 'Interacciones',
    IF(COUNT(participants_programs_logs.id) < 3,
        'baja',
        IF(COUNT(participants_programs_logs.id) > 3,
            'alta',
            'media')) AS 'NivelDeInteraccion',
    surveys_imports.name_import AS 'Grupo'
FROM
    participants_programs_logs
        INNER JOIN
    participant_surveys ON participants_programs_logs.participant_id = participant_surveys.participant_id
        INNER JOIN
    participants ON participants_programs_logs.participant_id = participants.id
        INNER JOIN
    surveys_imports ON participant_surveys.survey_id = surveys_imports.id
WHERE
    participants_programs_logs.program_id BETWEEN 95 AND 100
        AND participants_programs_logs.log_state = 'program'
GROUP BY participants_programs_logs.participant_id 
UNION SELECT 
    participants_programs_logs.participant_id AS 'idParticipante',
    CONCAT(participants.first_name,
            ' ',
            participants.last_name) AS 'NombreCompleto',
    participants.mobile_phone AS 'TelefonoMovil',
    IF(participants.gender = 'm',
        'Hombre',
        'Mujer') AS 'Genero',
    TIMESTAMPDIFF(YEAR,
        participants.birthday,
        CURDATE()) AS 'Edad',
    IF(TIMESTAMPDIFF(YEAR,
            participants.birthday,
            CURDATE()) BETWEEN 18 AND 29,
        'JÃ³venes (18-29)',
        IF(TIMESTAMPDIFF(YEAR,
                participants.birthday,
                CURDATE()) BETWEEN 30 AND 59,
            'Adultos (30-59)',
            IF(TIMESTAMPDIFF(YEAR,
                    participants.birthday,
                    CURDATE()) >= 60,
                'Adultos Mayores (60+)',
                'Fuera de Rango'))) AS 'RangoEdad',
    participants.town AS 'Municipio',
    IF(participants.neighborhood IS NULL,
        'SIN VEREDA',
        participants.neighborhood) AS 'Vereda',
    'digitall_mex_sprint2' AS 'Intervencion',
    6 AS 'OpcionesDisponibles',
    3 AS 'Mediana',
    COUNT(participants_programs_logs.id) AS 'Interacciones',
    IF(COUNT(participants_programs_logs.id) < 3,
        'baja',
        IF(COUNT(participants_programs_logs.id) > 3,
            'alta',
            'media')) AS 'NivelDeInteraccion',
    surveys_imports.name_import AS 'Grupo'
FROM
    participants_programs_logs
        INNER JOIN
    participant_surveys ON participants_programs_logs.participant_id = participant_surveys.participant_id
        INNER JOIN
    participants ON participants_programs_logs.participant_id = participants.id
        INNER JOIN
    surveys_imports ON participant_surveys.survey_id = surveys_imports.id
WHERE
    participants_programs_logs.program_id BETWEEN 115 AND 120
        AND participants_programs_logs.log_state = 'program'
GROUP BY participants_programs_logs.participant_id 
UNION SELECT 
    participants_programs_logs.participant_id AS 'idParticipante',
    CONCAT(participants.first_name,
            ' ',
            participants.last_name) AS 'NombreCompleto',
    participants.mobile_phone AS 'TelefonoMovil',
    IF(participants.gender = 'm',
        'Hombre',
        'Mujer') AS 'Genero',
    TIMESTAMPDIFF(YEAR,
        participants.birthday,
        CURDATE()) AS 'Edad',
    IF(TIMESTAMPDIFF(YEAR,
            participants.birthday,
            CURDATE()) BETWEEN 18 AND 29,
        'JÃ³venes (18-29)',
        IF(TIMESTAMPDIFF(YEAR,
                participants.birthday,
                CURDATE()) BETWEEN 30 AND 59,
            'Adultos (30-59)',
            IF(TIMESTAMPDIFF(YEAR,
                    participants.birthday,
                    CURDATE()) >= 60,
                'Adultos Mayores (60+)',
                'Fuera de Rango'))) AS 'RangoEdad',
    participants.town AS 'Municipio',
    IF(participants.neighborhood IS NULL,
        'SIN VEREDA',
        participants.neighborhood) AS 'Vereda',
    'comfama_col_sprint1' AS 'Intervencion',
    8 AS 'OpcionesDisponibles',
    4 AS 'Mediana',
    COUNT(participants_programs_logs.id) AS 'Interacciones',
    IF(COUNT(participants_programs_logs.id) < 4,
        'baja',
        IF(COUNT(participants_programs_logs.id) > 4,
            'alta',
            'media')) AS 'NivelDeInteraccion',
    surveys_imports.name_import AS 'Grupo'
FROM
    participants_programs_logs
        INNER JOIN
    participant_surveys ON participants_programs_logs.participant_id = participant_surveys.participant_id
        INNER JOIN
    participants ON participants_programs_logs.participant_id = participants.id
        INNER JOIN
    surveys_imports ON participant_surveys.survey_id = surveys_imports.id
WHERE
    participants_programs_logs.program_id BETWEEN 72 AND 79
        AND participants_programs_logs.log_state = 'program'
GROUP BY participants_programs_logs.participant_id 
UNION SELECT 
    participants_programs_logs.participant_id AS 'idParticipante',
    CONCAT(participants.first_name,
            ' ',
            participants.last_name) AS 'NombreCompleto',
    participants.mobile_phone AS 'TelefonoMovil',
    IF(participants.gender = 'm',
        'Hombre',
        'Mujer') AS 'Genero',
    TIMESTAMPDIFF(YEAR,
        participants.birthday,
        CURDATE()) AS 'Edad',
    IF(TIMESTAMPDIFF(YEAR,
            participants.birthday,
            CURDATE()) BETWEEN 18 AND 29,
        'JÃ³venes (18-29)',
        IF(TIMESTAMPDIFF(YEAR,
                participants.birthday,
                CURDATE()) BETWEEN 30 AND 59,
            'Adultos (30-59)',
            IF(TIMESTAMPDIFF(YEAR,
                    participants.birthday,
                    CURDATE()) >= 60,
                'Adultos Mayores (60+)',
                'Fuera de Rango'))) AS 'RangoEdad',
    participants.town AS 'Municipio',
    IF(participants.neighborhood IS NULL,
        'SIN VEREDA',
        participants.neighborhood) AS 'Vereda',
    'comfama_col_sprint2' AS 'Intervencion',
    7 AS 'OpcionesDisponibles',
    4 AS 'Mediana',
    COUNT(participants_programs_logs.id) AS 'Interacciones',
    IF(COUNT(participants_programs_logs.id) < 4,
        'baja',
        IF(COUNT(participants_programs_logs.id) > 4,
            'alta',
            'media')) AS 'NivelDeInteraccion',
    surveys_imports.name_import AS 'Grupo'
FROM
    participants_programs_logs
        INNER JOIN
    participant_surveys ON participants_programs_logs.participant_id = participant_surveys.participant_id
        INNER JOIN
    participants ON participants_programs_logs.participant_id = participants.id
        INNER JOIN
    surveys_imports ON participant_surveys.survey_id = surveys_imports.id
WHERE
    participants_programs_logs.program_id BETWEEN 88 AND 94
        AND participants_programs_logs.log_state = 'program'
GROUP BY participants_programs_logs.participant_id 
UNION SELECT 
    participants_programs_logs.participant_id AS 'idParticipante',
    CONCAT(participants.first_name,
            ' ',
            participants.last_name) AS 'NombreCompleto',
    participants.mobile_phone AS 'TelefonoMovil',
    IF(participants.gender = 'm',
        'Hombre',
        'Mujer') AS 'Genero',
    TIMESTAMPDIFF(YEAR,
        participants.birthday,
        CURDATE()) AS 'Edad',
    IF(TIMESTAMPDIFF(YEAR,
            participants.birthday,
            CURDATE()) BETWEEN 18 AND 29,
        'JÃ³venes (18-29)',
        IF(TIMESTAMPDIFF(YEAR,
                participants.birthday,
                CURDATE()) BETWEEN 30 AND 59,
            'Adultos (30-59)',
            IF(TIMESTAMPDIFF(YEAR,
                    participants.birthday,
                    CURDATE()) >= 60,
                'Adultos Mayores (60+)',
                'Fuera de Rango'))) AS 'RangoEdad',
    participants.town AS 'Municipio',
    IF(participants.neighborhood IS NULL,
        'SIN VEREDA',
        participants.neighborhood) AS 'Vereda',
    'comfama_col_sprint3' AS 'Intervencion',
    7 AS 'OpcionesDisponibles',
    4 AS 'Mediana',
    COUNT(participants_programs_logs.id) AS 'Interacciones',
    IF(COUNT(participants_programs_logs.id) < 4,
        'baja',
        IF(COUNT(participants_programs_logs.id) > 4,
            'alta',
            'media')) AS 'NivelDeInteraccion',
    surveys_imports.name_import AS 'Grupo'
FROM
    participants_programs_logs
        INNER JOIN
    participant_surveys ON participants_programs_logs.participant_id = participant_surveys.participant_id
        INNER JOIN
    participants ON participants_programs_logs.participant_id = participants.id
        INNER JOIN
    surveys_imports ON participant_surveys.survey_id = surveys_imports.id
WHERE
    participants_programs_logs.program_id BETWEEN 121 AND 127
        AND participants_programs_logs.log_state = 'program'
GROUP BY participants_programs_logs.participant_id
UNION SELECT 
    participants_programs_logs.participant_id AS 'idParticipante',
    CONCAT(participants.first_name,
            ' ',
            participants.last_name) AS 'NombreCompleto',
    participants.mobile_phone AS 'TelefonoMovil',
    IF(participants.gender = 'm',
        'Hombre',
        'Mujer') AS 'Genero',
    TIMESTAMPDIFF(YEAR,
        participants.birthday,
        CURDATE()) AS 'Edad',
    IF(TIMESTAMPDIFF(YEAR,
            participants.birthday,
            CURDATE()) BETWEEN 18 AND 29,
        'JÃ³venes (18-29)',
        IF(TIMESTAMPDIFF(YEAR,
                participants.birthday,
                CURDATE()) BETWEEN 30 AND 59,
            'Adultos (30-59)',
            IF(TIMESTAMPDIFF(YEAR,
                    participants.birthday,
                    CURDATE()) >= 60,
                'Adultos Mayores (60+)',
                'Fuera de Rango'))) AS 'RangoEdad',
    participants.town AS 'Municipio',
    IF(participants.neighborhood IS NULL,
        'SIN VEREDA',
        participants.neighborhood) AS 'Vereda',
    'digitall_col_sprint2' AS 'Intervencion',
    8 AS 'OpcionesDisponibles',
    4 AS 'Mediana',
    COUNT(participants_programs_logs.id) AS 'Interacciones',
    IF(COUNT(participants_programs_logs.id) < 4,
        'baja',
        IF(COUNT(participants_programs_logs.id) > 4,
            'alta',
            'media')) AS 'NivelDeInteraccion',
    surveys_imports.name_import AS 'Grupo'
FROM
    participants_programs_logs
        INNER JOIN
    participant_surveys ON participants_programs_logs.participant_id = participant_surveys.participant_id
        INNER JOIN
    participants ON participants_programs_logs.participant_id = participants.id
        INNER JOIN
    surveys_imports ON participant_surveys.survey_id = surveys_imports.id
WHERE
    participants_programs_logs.program_id BETWEEN 129 AND 136
        AND participants_programs_logs.log_state = 'program'
GROUP BY participants_programs_logs.participant_id
UNION SELECT 
    participants_programs_logs.participant_id AS 'idParticipante',
    CONCAT(participants.first_name,
            ' ',
            participants.last_name) AS 'NombreCompleto',
    participants.mobile_phone AS 'TelefonoMovil',
    IF(participants.gender = 'm',
        'Hombre',
        'Mujer') AS 'Genero',
    TIMESTAMPDIFF(YEAR,
        participants.birthday,
        CURDATE()) AS 'Edad',
    IF(TIMESTAMPDIFF(YEAR,
            participants.birthday,
            CURDATE()) BETWEEN 18 AND 29,
        'JÃ³venes (18-29)',
        IF(TIMESTAMPDIFF(YEAR,
                participants.birthday,
                CURDATE()) BETWEEN 30 AND 59,
            'Adultos (30-59)',
            IF(TIMESTAMPDIFF(YEAR,
                    participants.birthday,
                    CURDATE()) >= 60,
                'Adultos Mayores (60+)',
                'Fuera de Rango'))) AS 'RangoEdad',
    participants.town AS 'Municipio',
    IF(participants.neighborhood IS NULL,
        'SIN VEREDA',
        participants.neighborhood) AS 'Vereda',    
    'idrc_col_sprint3' AS 'Intervencion', -- EDIT THIS
    7 AS 'OpcionesDisponibles', -- EDIT THIS
    4 AS 'Mediana', -- EDIT THIS
    COUNT(participants_programs_logs.id) AS 'Interacciones',
    IF(COUNT(participants_programs_logs.id) < 4, -- EDIT THIS
        'baja',
        IF(COUNT(participants_programs_logs.id) > 4, -- EDIT THIS
            'alta',
            'media')) AS 'NivelDeInteraccion',
    surveys_imports.name_import AS 'Grupo'
FROM
    participants_programs_logs
        INNER JOIN
    participant_surveys ON participants_programs_logs.participant_id = participant_surveys.participant_id
        INNER JOIN
    participants ON participants_programs_logs.participant_id = participants.id
        INNER JOIN
    surveys_imports ON participant_surveys.survey_id = surveys_imports.id
WHERE
    participants_programs_logs.program_id BETWEEN 185 AND 191 -- EDIT THIS
        AND participants_programs_logs.log_state = 'program'
GROUP BY participants_programs_logs.participant_id;