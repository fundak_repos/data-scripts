SELECT participants_programs_logs.id AS 'id', participants.id AS 'idParticipante', participants.document AS 'Documento', 
CONCAT(participants.first_name, ' ', participants.last_name) AS 'NombreCompleto',
IF(participants.mobile_phone IS NULL, 'SIN TELEFONO', participants.mobile_phone) AS 'TelefonoMovil',
IF(participants.gender = "m", "Hombre", "Mujer") AS 'Genero',
TIMESTAMPDIFF(YEAR, participants.birthday, CURDATE()) AS 'Edad',
IF(TIMESTAMPDIFF(YEAR, participants.birthday, CURDATE()) BETWEEN 18 AND 29, 'Jóvenes (18-29)', IF(TIMESTAMPDIFF(YEAR, participants.birthday, CURDATE()) BETWEEN 30 AND 59, 'Adultos (30-59)', IF(TIMESTAMPDIFF(YEAR, participants.birthday, CURDATE()) >= 60, 'Adultos Mayores (60+)', 'Fuera de Rango'))) AS 'RangoEdad',
participants.family_id AS 'Familia',
IF(participants.is_contact = 1, 'Si', 'No') AS 'EsContacto',
participants.town AS 'Municipio', IF(participants.neighborhood IS NULL, 'SIN VEREDA', participants.neighborhood) AS 'Vereda',
programs.id AS 'idOferta', programs.name AS 'NombreOferta', programs.coverage AS 'Sprint',
programs_costs.total_steps AS 'TotalPasos', programs_costs.total_cost AS 'CostoTotal',
participants_programs_logs.log_state AS 'Tipo',
DATE_FORMAT(CONVERT_TZ(participants_programs_logs.created_at, '+00:00', '-05:00'), '%Y%m%d') AS 'Registro',
DATE_FORMAT(CONVERT_TZ(participants_programs_logs.created_at, '+00:00', '-05:00'), '%H') AS 'RegistroHora',
surveys_imports.name_import AS 'Grupo'
FROM participants_programs_logs
    INNER JOIN participants ON participants_programs_logs.participant_id = participants.id
    INNER JOIN programs ON participants_programs_logs.program_id = programs.id
    INNER JOIN program_steps ON participants_programs_logs.step_id = program_steps.id
    INNER JOIN participant_surveys ON participants_programs_logs.participant_id = participant_surveys.participant_id
    INNER JOIN surveys_imports ON participant_surveys.survey_id = surveys_imports.id
    INNER JOIN 
    (
        SELECT program_steps.program_id AS id, programs.name AS program_name,
        COUNT(program_steps.id) AS total_steps, COUNT(program_steps.id) * 0.00076 AS total_cost
        FROM program_steps
            INNER JOIN programs ON program_steps.program_id = programs.id
        GROUP BY program_steps.program_id
    ) AS programs_costs ON participants_programs_logs.program_id = programs_costs.id
WHERE participants_programs_logs.log_state = 'program'
GROUP BY participants_programs_logs.id;