SELECT -- *
participants.id, participants.document, participants.family_id, first_name, participants.last_name, participants.mobile_phone, participants.complete_mobile_phone, surveys_imports.id, surveys_imports.name_import
FROM participants

	INNER JOIN participant_surveys ON participants.id = participant_surveys.participant_id
	INNER JOIN surveys_imports ON participant_surveys.survey_id = surveys_imports.id

-- WHERE family_id LIKE 'COMFAMA-FACILITADORES'
-- SELECT family_id FROM participants GROUP BY family_id
-- WHERE participants.id = 10204
-- WHERE participants.id IN (3,12,14,389)
-- WHERE participants.id IN (3,4,7,8,10,12,14,16,17,389)
-- WHERE participants.id IN (10,12,14,389)
WHERE first_name LIKE 'EMILIE'
-- WHERE participants.id IN (3866,3990)
-- WHERE participants.id BETWEEN 4237 AND 5230
-- WHERE participants.participant = 1093 
-- WHERE mobile_phone LIKE '933%'
-- WHERE surveys_imports.name_import LIKE '%conred%'
-- WHERE participants.id IN (14,7,12,389)
-- WHERE mobile_phone = '3154219438'
-- WHERE mobile_phone IN ('17713156539','15524008758','15535086432')
-- WHERE participants.id IN ('3997','4147','4148','4149','4150')
ORDER BY participants.id ASC
LIMIT 0, 10000;