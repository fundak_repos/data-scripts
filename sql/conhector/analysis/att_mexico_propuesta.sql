select created_at, date_format(created_at, '%Y-%m-%d %H:%i:00') from participants_programs_logs

select * from surveys_imports

####Cuantas personas interactuan por dia en cada camapana
with interaction_day as (
Select distinct   ppl.participant_id, date(ppl.created_at) as activity_date, lower(p.type) as campaign 
from participants_programs_logs ppl 
INNER JOIN programs p ON ppl.program_id = p.id
 INNER JOIN participant_surveys ON ppl .participant_id = participant_surveys.participant_id
  INNER JOIN surveys_imports ON participant_surveys.survey_id = surveys_imports.id
  WHERE surveys_imports.name_import LIKE 'participants%'

 order by p.type, ppl.participant_id, date(ppl.created_at)
 )
 
 select  activity_date,
 COUNT(CASE WHEN campaign ='covid' THEN 1 END) AS covid,
 COUNT(CASE WHEN campaign like '%digitall%' THEN 1 END) AS digitall,
 COUNT(CASE WHEN campaign='comfama' THEN 1 END) AS comfama
 from interaction_day
 group by  activity_date
 
 
 
 ####Cuantas personas interactuan al mismo tiempo
with interaction_hour as (
Select distinct   ppl.participant_id, date_format(ppl.created_at, '%Y-%m-%d %H:00:00') as activity_hour, lower(p.type) as campaign 
from participants_programs_logs ppl 
 INNER JOIN programs p ON ppl.program_id = p.id
 INNER JOIN participant_surveys ON ppl .participant_id = participant_surveys.participant_id
  INNER JOIN surveys_imports ON participant_surveys.survey_id = surveys_imports.id
  WHERE surveys_imports.name_import LIKE 'participants%'
 order by date_format(ppl.created_at, '%Y-%m-%d %H:00:00'), p.type, ppl.participant_id 
 )
 
 select activity_hour, count(*) as people_per_hour_all,
  COUNT(CASE WHEN campaign ='covid' THEN 1 END) AS people_per_hour_covid,
 COUNT(CASE WHEN campaign like '%digitall%' THEN 1 END) AS people_per_hour_digitall,
 COUNT(CASE WHEN campaign='comfama' THEN 1 END) AS people_per_hour_comfama
 from interaction_hour
 group by  activity_hour
 order by people_per_hour_all desc
 
 
  ####franjas horarias
  
 
 
 with franjas_horarias as (
Select distinct ppl.participant_id, date(ppl.created_at),  hour(ppl.created_at) as activity_hour,
dayofweek(ppl.created_at) as dayofweek, lower(p.type) as campaign from participants_programs_logs ppl 
 INNER JOIN programs p ON ppl.program_id = p.id
 INNER JOIN participant_surveys ON ppl .participant_id = participant_surveys.participant_id
  INNER JOIN surveys_imports ON participant_surveys.survey_id = surveys_imports.id
  WHERE surveys_imports.name_import LIKE 'participants%'
 order by p.type, ppl.participant_id, activity_hour
 )
 
 
 select count(*) as total_users, case when dayofweek=1 or dayofweek=7 THEN 'weekend' ELSE 'weekday' END AS day_type, activity_hour
 from franjas_horarias
 group by day_type, activity_hour
 order by total_users desc
 
 
