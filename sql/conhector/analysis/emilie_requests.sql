-# of first-time users: think of how to define better
select * from participants
select count(*) from participants;
select count(*)
from participants p
LEFT JOIN participants_programs_logs ppl on p.id=ppl.participant_id
where ppl.participant_id is NULL

select * from participants_programs pp 

select * from programs

##alternative to first-time users: drop-off rate. Users that started a session but did not finish it


with activity as (
Select date(ppl.created_at) as activity_date, ppl.participant_id as participant_id_activity from participants_programs_logs ppl
INNER JOIN participant_surveys ON ppl.participant_id = participant_surveys.participant_id
INNER JOIN surveys_imports ON participant_surveys.survey_id = surveys_imports.id
WHERE  log_state<>'session' AND surveys_imports.name_import LIKE 'participants%'

),

count_cte as (
Select 
count(a.participant_id_activity) as activity_count, 
  count(ppl.participant_id) as total_count  
from participants_programs_logs ppl
left join activity  a on ppl.participant_id =a.participant_id_activity and date(ppl.created_at)=a.activity_date
INNER JOIN participant_surveys ON ppl.participant_id = participant_surveys.participant_id
INNER JOIN surveys_imports ON participant_surveys.survey_id = surveys_imports.id
WHERE log_state='session' AND surveys_imports.name_import LIKE 'participants%'
)



SELECT 1 - (activity_count/ total_count) as drop_off_percentage from count_cte
###drop off percentage 0.0182


###############################################
##Repeat Users
# of repeat participants ( # of participants that were selected more than once to our programs 

##this tells you the min and max program id per type and how many programs there where. 
##this is our base to figure out how many programs a person has completed
#DONE
with program_type as (
select type, coverage, min(id) as min_id, count(id) as total_programs, max(id) as max_id from programs 
group by type, coverage),



programs_consumed_deduped as (
SELECT DISTINCT ppl.participant_id, program_id, type, coverage 
FROM participants_programs_logs ppl
    #INNER JOIN participants ON participants_programs_logs.participant_id = participants.id
    INNER JOIN programs ON ppl.program_id = programs.id
    INNER JOIN participant_surveys ON ppl.participant_id = participant_surveys.participant_id
	INNER JOIN surveys_imports ON participant_surveys.survey_id = surveys_imports.id
	WHERE   surveys_imports.name_import LIKE 'participants%' and
    log_state='program' 
    order by ppl.participant_id, type, coverage, program_id)
    
    
###WAITING FOR ABRAHAM TO ANSWER QUESTION ABOUT the 304 count for this table to proceed

 Select count(*) from surveys_imports si  
 select count(*) from programs
    
 Select count(*) from participants_programs pp
 left join programs_consumed_deduped pd on pp.participant_id=pd.participant_id AND pd.program_id=pp.participant_id
    
#Conversion Rate ( the total # of participants that has engaged at least once with ConHector / the total # of participants that were selected for our programs )    
###WAITING FOR ABRAHAM TO ANSWER QUESTION ABOUT the 304 count for this table to proceed

 
 
###The average number of chat sessions for repeat users

with repeat_sessions as (Select ppl.participant_id, c.name as country ,count(*) as total_sessions
from participants_programs_logs ppl
INNER JOIN participant_surveys ON ppl.participant_id = participant_surveys.participant_id
INNER JOIN surveys_imports ON participant_surveys.survey_id = surveys_imports.id
LEFT JOIN participants p ON ppl.participant_id=p.id 
LEFT JOIN countries c ON p.country_id=c.id
WHERE log_state='session' AND surveys_imports.name_import LIKE 'participants%'
group by country, ppl.participant_id
HAVING total_sessions > 1
order by total_sessions)

SELECT country, avg(total_sessions), min(total_sessions), max(total_sessions) from repeat_sessions
group by country


#Chatbot handoff rate (the total # of times that users have used the "HELP" option to talk to a human on overall chat sessions)

select c.name as country,   count(*) 
#select distinct ppl.description
from participants_programs_logs ppl
inner join programs p on ppl.program_id=p.id
INNER JOIN participant_surveys ON ppl .participant_id = participant_surveys.participant_id
INNER JOIN surveys_imports ON participant_surveys.survey_id = surveys_imports.id
LEFT JOIN participants ps ON ppl.participant_id=ps.id 
LEFT JOIN countries c ON p.country_id=c.id
##where ppl.description LIKE '%ayuda'
where ppl.description in ('ayuda',
'5)_ayuda',
'4_ayuda',
'2_ayuda',
'Quiero_una_ayuda',
'3_ayuda',
'.ayuda',
'2_)_ayuda',
'9)_ayuda',
'5_ayuda',
'6_ayuda',
'7_ayuda',
'4)_ayuda')
AND surveys_imports.name_import LIKE 'participants%'
group by country
order by ppl.description



###Simultaneous opened chat sessions (maximum number of chat sessions that were opened simultaneously)


####Cuantas personas interactuan al mismo tiempo
with interaction_minute as (
Select    ppl.participant_id, date_format(ppl.created_at, '%Y-%m-%d %H:%i:00') as activity_minute, c.name as country 
from participants_programs_logs ppl 
 #INNER JOIN programs p ON ppl.program_id = p.id
 INNER JOIN participant_surveys ON ppl .participant_id = participant_surveys.participant_id
  INNER JOIN surveys_imports ON participant_surveys.survey_id = surveys_imports.id
  LEFT JOIN participants p ON ppl.participant_id=p.id 
LEFT JOIN countries c ON p.country_id=c.id
  WHERE surveys_imports.name_import LIKE 'participants%'
 order by date_format(ppl.created_at, '%Y-%m-%d %H:%i:00'),  ppl.participant_id 
 )
 
 select activity_minute, count(*) as people_per_minute_all,
  COUNT(CASE WHEN country ='Colombia' THEN 1 END) AS people_per_minute_colombia,
 COUNT(CASE WHEN country='Mexico' THEN 1 END) AS people_per_minute_Mexico,
 COUNT(CASE WHEN country='Peru' THEN 1 END) AS people_per_minute_Peru
 from interaction_minute
 group by  activity_minute
 order by people_per_minute_all desc
 
 
 
  
  
  
 #####No interactions

  
  with no_interact as (
  SELECT participants_programs_logs.participant_id 
FROM participants_programs_logs
    INNER JOIN participants ON participants_programs_logs.participant_id = participants.id
    INNER JOIN programs ON participants_programs_logs.program_id = programs.id
    INNER JOIN program_steps ON participants_programs_logs.step_id = program_steps.id
    INNER JOIN participant_surveys ON participants_programs_logs.participant_id = participant_surveys.participant_id
    INNER JOIN surveys_imports ON participant_surveys.survey_id = surveys_imports.id
    
WHERE participants_programs_logs.log_state = 'session'
    AND participants_programs_logs.participant_id NOT IN (SELECT participants_programs_logs.participant_id FROM participants_programs_logs WHERE participants_programs_logs.log_state = 'program') and surveys_imports.name_import LIKE 'participants%'
   ),
   
   
 unique_users as (  
 select distinct participants_programs_logs.participant_id, c.name as country  FROM participants_programs_logs  
 INNER JOIN participant_surveys ON participants_programs_logs.participant_id = participant_surveys.participant_id
  INNER JOIN surveys_imports ON participant_surveys.survey_id = surveys_imports.id
  LEFT JOIN participants p ON participants_programs_logs.participant_id=p.id 
LEFT JOIN countries c ON p.country_id=c.id
  WHERE surveys_imports.name_import LIKE 'participants%'
 )
   
select 
#country,
count(*) as total,
count(n.participant_id) as no_interact
FROM unique_users u
LEFT JOIN no_interact n on u.participant_id=n.participant_id
#GROUP BY country


#Retention Rate ( the total # of participants that has consulted ConHector more than once on repeated occasions / the total # of participants that were selected more than once to our programs)

select  c.name as country, count(*) FROM participants 
INNER JOIN participant_surveys ON participants.id = participant_surveys.participant_id
INNER JOIN surveys_imports ON participant_surveys.survey_id = surveys_imports.id
LEFT JOIN countries c ON participants.country_id=c.id
WHERE surveys_imports.name_import LIKE 'participants%'
group by country


with all_participants as (
select participants.*, c.name as country FROM participants 
INNER JOIN participant_surveys ON participants.id = participant_surveys.participant_id
INNER JOIN surveys_imports ON participant_surveys.survey_id = surveys_imports.id
LEFT JOIN countries c ON participants.country_id=c.id
WHERE surveys_imports.name_import LIKE 'participants%'),




 distinct_week as (
select distinct participant_id ,week(created_at) as myweek from participants_programs_logs ppl 
order by participant_id),



group_by_week as (
SELECT 
    participant_id,
    
    ROW_NUMBER() OVER (PARTITION BY participant_id ORDER BY participant_id, myweek) AS week_num
FROM distinct_week

),

mygroup as (
select ap.id, ap.country,
case when week_num=1 then 1 else 0 end as week1,
case when week_num=2 then 1 else 0 end as week2,
case when week_num=3 then 1 else 0 end as week3,
case when week_num=4 then 1 else 0 end as week4,
case when week_num=5 then 1 else 0 end as week5,
case when week_num=6 then 1 else 0 end as week6,
case when week_num=7 then 1 else 0 end as week7,
case when week_num=8 then 1 else 0 end as week8,
case when week_num=9 then 1 else 0 end as week9,
case when week_num=10 then 1 else 0 end as week10,
case when week_num=11 then 1 else 0 end as week11,
case when week_num=12 then 1 else 0 end as week12,
case when week_num=13 then 1 else 0 end as week13,
case when week_num=14 then 1 else 0 end as week14,
case when week_num=15 then 1 else 0 end as week15,
case when week_num=16 then 1 else 0 end as week16,
case when week_num=17 then 1 else 0 end as week17,
case when week_num=18 then 1 else 0 end as week18,
case when week_num=19 then 1 else 0 end as week19,
case when week_num=20 then 1 else 0 end as week20,
case when week_num=21 then 1 else 0 end as week21,
case when week_num=22 then 1 else 0 end as week22,
case when week_num=23 then 1 else 0 end as week23,
case when week_num=24 then 1 else 0 end as week24,
case when week_num=25 then 1 else 0 end as week25,
case when week_num=26 then 1 else 0 end as week26,
case when week_num=27 then 1 else 0 end as week27,
case when week_num=28 then 1 else 0 end as week28,
case when week_num=29 then 1 else 0 end as week29,
case when week_num=30 then 1 else 0 end as week30,
case when week_num=31 then 1 else 0 end as week31,
case when week_num=32 then 1 else 0 end as week32,
case when week_num=33 then 1 else 0 end as week33,
case when week_num=34 then 1 else 0 end as week34,
case when week_num=35 then 1 else 0 end as week35,
case when week_num=36 then 1 else 0 end as week36,
case when week_num=37 then 1 else 0 end as week37,
case when week_num=38 then 1 else 0 end as week38,
case when week_num=39 then 1 else 0 end as week39,
case when week_num=40 then 1 else 0 end as week40,
case when week_num=41 then 1 else 0 end as week41,
case when week_num=42 then 1 else 0 end as week42,
case when week_num=43 then 1 else 0 end as week43,
case when week_num=44 then 1 else 0 end as week44,
case when week_num=45 then 1 else 0 end as week45,
case when week_num=46 then 1 else 0 end as week46,
case when week_num=47 then 1 else 0 end as week47,
case when week_num=48 then 1 else 0 end as week48,
case when week_num=49 then 1 else 0 end as week49,
case when week_num=50 then 1 else 0 end as week50,
case when week_num=51 then 1 else 0 end as week51,
case when week_num=52 then 1 else 0 end as week52
from all_participants ap
 left join group_by_week gw ON ap.id=gw.participant_id
#group by participant_id
order by ap.id)

Select country,
count(*),
sum(week1) as week1,
sum(week2) as week2,
sum(week3) as week3,
sum(week4) as week4,
sum(week5) as week5,
sum(week6) as week6,
sum(week7) as week7,
sum(week8) as week8,
sum(week9) as week9,
sum(week10) as week10,
sum(week11) as week11,
sum(week12) as week12,
sum(week13) as week13,
sum(week14) as week14,
sum(week15) as week15,
sum(week16) as week16,
sum(week17) as week17,
sum(week18) as week18,
sum(week19) as week19,
sum(week20) as week20,
sum(week21) as week21,
sum(week22) as week22,
sum(week23) as week23,
sum(week24) as week24,
sum(week25) as week25,
sum(week26) as week26,
sum(week27) as week27,
sum(week28) as week28,
sum(week29) as week29,
sum(week30) as week30,
sum(week31) as week31,
sum(week32) as week32,
sum(week33) as week33,
sum(week34) as week34,
sum(week35) as week35,
sum(week36) as week36,
sum(week37) as week37,
sum(week38) as week38,
sum(week39) as week39,
sum(week40) as week40,
sum(week41) as week41,
sum(week42) as week42,
sum(week43) as week43,
sum(week44) as week44,
sum(week45) as week45,
sum(week46) as week46,
sum(week47) as week47,
sum(week48) as week48,
sum(week49) as week49,
sum(week50) as week50,
sum(week51) as week51,
sum(week52) as week52
from mygroup
group by country 
 









