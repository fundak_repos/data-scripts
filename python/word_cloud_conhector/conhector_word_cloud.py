import pandas as pd
from wordcloud import WordCloud, STOPWORDS 
import nltk
from nltk.corpus import stopwords
from nltk import word_tokenize
from nltk.data import load
from nltk.stem import SnowballStemmer
from string import punctuation
import re
import matplotlib.pyplot as plt

#matplotlib.style.use('ggplotppd.options.mode.chained_assignment = None


 #stopword list to use
spanish_stopwords = stopwords.words('spanish')

stemmer = SnowballStemmer('spanish')

def stem_tokens(tokens, stemmer):
    stemmed = []
    for item in tokens:
        stemmed.append(stemmer.stem(item))
    return stemmed

# Define a function to plot word cloud
def plot_cloud(wordcloud):
    # Set figure size
    plt.figure(figsize=(40, 30))
    # Display image
    plt.imshow(wordcloud) 
    # No axis details
    plt.axis("off")
    #plt.show()

df=pd.read_csv('conhector_buzon_de_sugerencias.csv')
is_comfama=df['Campana']=='COMFAMA'
comfama_df=df[is_comfama]

print(comfama_df)

respuesta=df['respuesta']

grouped = df.groupby("pregunta_recode_numero")



#print(respuesta)
#print(type(respuesta))
text=""
for items in respuesta.iteritems(): 
    #print(items[1])
    text=text + " " +  items[1]

# Clean text
text = re.sub(r'==.*?==+', '', text)
text = text.replace('\n', '')
text=text.lower()
#print(text)

def tokenize(text):
        """
        Tokenizes sequences of text and stems the tokens.
        :param text: String to tokenize
        :return: List with stemmed tokens
        """
        tokens = nltk.WhitespaceTokenizer().tokenize(text)
        tokens = list(set(re.sub("[^a-zA-Z\']", "", token) for token in tokens))
        tokens = [word for word in tokens if word not in stopwords.words('spanish')]
        tokens = list(set(re.sub("[^a-zA-Z]", "", token) for token in tokens))
        stems = []
        stemmer = SnowballStemmer("spanish")
        for token in tokens:
            token = stemmer.stem(token)
            if token != "":
                stems.append(token)
        return stems 
stemmer = SnowballStemmer("spanish")
text1=tokenize(text)
text2=" ".join(str(x) for x in text1)


# Generate word cloud
wordcloud = WordCloud(width = 3000, height = 2000, random_state=1, background_color='salmon', colormap='Pastel1', collocations=False, stopwords = spanish_stopwords).generate(text2)
# Plot
plot_cloud(wordcloud)

# Save image
wordcloud.to_file("wordcloud_stemmed_general.png")


wordcloud = WordCloud(width = 3000, height = 2000, random_state=1, background_color='salmon', colormap='Pastel1', collocations=False, stopwords = spanish_stopwords).generate(text)
# Plot
plot_cloud(wordcloud)

# Save image
wordcloud.to_file("wordcloud_general.png")

####alll



for x in range(1,8):
    grouped_df=grouped.get_group(x)
    respuesta=grouped_df['respuesta'] 
    #print("break")
    #print(grouped_df['pregunta_recode'])   
    
    
    text=""
    for items in respuesta.iteritems(): 
        #print(items[1])
        text=text + " " +  items[1]

    # Clean text
    text = re.sub(r'==.*?==+', '', text)
    text = text.replace('\n', '')
    text=text.lower()  


    text1=tokenize(text)
    text2=" ".join(str(x) for x in text1)


    # Generate word cloud
    wordcloud = WordCloud(width = 3000, height = 2000, random_state=1, background_color='salmon', colormap='Pastel1', collocations=False, stopwords = spanish_stopwords).generate(text2)
    # Plot
    plot_cloud(wordcloud)

    # Save image
    myfile=f"""wordcloud_stemmed_{x}.png"""
    wordcloud.to_file(myfile)
    plt.close('all')

    wordcloud = WordCloud(width = 3000, height = 2000, random_state=1, background_color='salmon', colormap='Pastel1', collocations=False, stopwords = spanish_stopwords).generate(text)
    # Plot
    plot_cloud(wordcloud)
    

    # Save image
    myfile=f"""wordcloud_{x}.png"""
    wordcloud.to_file(myfile)
    plt.close('all')

  



####COMFAMA


for x in range(1,8):
    grouped_df=grouped.get_group(x)
    is_comfama=grouped_df['Campana']=='COMFAMA'
    comfama_df=grouped_df[is_comfama]
    print(comfama_df)
    print(x)
    respuesta=comfama_df['respuesta'] 
    count_row = respuesta.shape[0]
    if count_row > 1:
        #print("break")
        #print(grouped_df['pregunta_recode'])   
    
    
        text=""
        for items in respuesta.iteritems(): 
            #print(items[1])
            text=text + " " +  items[1]

        # Clean text
        text = re.sub(r'==.*?==+', '', text)
        text = text.replace('\n', '')
        text=text.lower()  


        text1=tokenize(text)
        text2=" ".join(str(x) for x in text1)


        # Generate word cloud
        wordcloud = WordCloud(width = 3000, height = 2000, random_state=1, background_color='salmon', colormap='Pastel1', collocations=False, stopwords = spanish_stopwords).generate(text2)
        # Plot
        plot_cloud(wordcloud)
    
        # Save image
        myfile=f"""wordcloud_stemmed_comfama_{x}.png"""
        wordcloud.to_file(myfile)
        plt.close('all')

        wordcloud = WordCloud(width = 3000, height = 2000, random_state=1, background_color='salmon', colormap='Pastel1', collocations=False, stopwords = spanish_stopwords).generate(text)
        # Plot
        plot_cloud(wordcloud)

        # Save image
        myfile=f"""wordcloud_comfama_{x}.png"""
        wordcloud.to_file(myfile)
        plt.close('all')
    else:
        pass
        

####COVID


for x in range(1,8):
    grouped_df=grouped.get_group(x)
    is_covid=grouped_df['Campana']=='COVID-19'
    covid_df=grouped_df[is_covid]
    print(covid_df)
    print(x)
    respuesta=covid_df['respuesta'] 
    count_row = respuesta.shape[0]
    if count_row > 1:
        #print("break")
        #print(grouped_df['pregunta_recode'])   
    
    
        text=""
        for items in respuesta.iteritems(): 
            #print(items[1])
            text=text + " " +  items[1]

        # Clean text
        text = re.sub(r'==.*?==+', '', text)
        text = text.replace('\n', '')
        text=text.lower()  


        text1=tokenize(text)
        text2=" ".join(str(x) for x in text1)


        # Generate word cloud
        wordcloud = WordCloud(width = 3000, height = 2000, random_state=1, background_color='salmon', colormap='Pastel1', collocations=False, stopwords = spanish_stopwords).generate(text2)
        # Plot
        plot_cloud(wordcloud)
    
        # Save image
        myfile=f"""wordcloud_stemmed_covid_{x}.png"""
        wordcloud.to_file(myfile)
        plt.close('all')

        wordcloud = WordCloud(width = 3000, height = 2000, random_state=1, background_color='salmon', colormap='Pastel1', collocations=False, stopwords = spanish_stopwords).generate(text)
        # Plot
        plot_cloud(wordcloud)

        # Save image
        myfile=f"""wordcloud_covid_{x}.png"""
        wordcloud.to_file(myfile)
        plt.close('all')
    else:
        pass
        

####DIGTALL


for x in range(1,8):
    grouped_df=grouped.get_group(x)
    is_digtall=grouped_df['Campana']=='DIGITALL'
    digitall_df=grouped_df[is_digtall]
    print(digitall_df)
    print(x)
    respuesta=digitall_df['respuesta'] 
    count_row = respuesta.shape[0]
    if count_row > 1:
        #print("break")
        #print(grouped_df['pregunta_recode'])   
    
    
        text=""
        for items in respuesta.iteritems(): 
            #print(items[1])
            text=text + " " +  items[1]

        # Clean text
        text = re.sub(r'==.*?==+', '', text)
        text = text.replace('\n', '')
        text=text.lower()  


        text1=tokenize(text)
        text2=" ".join(str(x) for x in text1)


        # Generate word cloud
        wordcloud = WordCloud(width = 3000, height = 2000, random_state=1, background_color='salmon', colormap='Pastel1', collocations=False, stopwords = spanish_stopwords).generate(text2)
        # Plot
        plot_cloud(wordcloud)
    
        # Save image
        myfile=f"""wordcloud_stemmed_digit_{x}.png"""
        wordcloud.to_file(myfile)
        plt.close('all')

        wordcloud = WordCloud(width = 3000, height = 2000, random_state=1, background_color='salmon', colormap='Pastel1', collocations=False, stopwords = spanish_stopwords).generate(text)
        # Plot
        plot_cloud(wordcloud)

        # Save image
        myfile=f"""wordcloud_digit_{x}.png"""
        wordcloud.to_file(myfile)
        plt.close('all')
    else:
        pass
         
        
            
        
    



